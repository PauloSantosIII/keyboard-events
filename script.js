'use strict'

let boxTop = 150
let boxLeft = 350

document.addEventListener('keydown', (event) => {
  const keyName = event.key

  if (keyName === 'ArrowUp') {
      boxTop -= 10
  } else if (keyName === 'ArrowDown') {
      boxTop += 10
  } else if (keyName === 'ArrowLeft') {
      boxLeft -= 10
  } else if (keyName === 'ArrowRight') {
      boxLeft += 10      
  }
  document.getElementById('box').style.top = `${boxTop}px`
  document.getElementById('box').style.left = `${boxLeft}px`
  gol()
});

function gol() {
    if (boxTop == 150 && boxLeft == -60) {
        let audio = document.getElementById('torcida')
        audio.src = './audios/gol.mp3'
    } else if (boxTop == 150 && boxLeft == 760) {
        let audio = document.getElementById('torcida')
        audio.src = './audios/gol.mp3'
    }
    setTimeout(() => {
        document.location.reload(true)
    }, 9600)
}